mode(-1)
// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
mode(3)
messagebox(['Working with the non linear constraint problem'
	    'Cost: x2*exp(x1+x3)+(x3*x4)^2+(x3-x5)^2'
	    'Constraints'
	    '      x1+x2-10=0'
	    '     -x1-x3+x4=0'
	    '     -x2+x3+x5=0'
	    '     -x4-x5+10=0'
	    'see http://www.netlib.org/opt/lsnno/'],'Problem definition','info')
path=get_sif_path()+'sif';    //get the SIF database path
siffile=path+'/LSNNODOC.SIF'; //select the LSNNODOC.SIF problem
probpath=TMPDIR+'/LSNNODOC';  //set the directory where to create generated files
sifdecode(siffile,probpath)   //build the fortran files defining the problem
buildprob(probpath)           // compile and link the generated files
halt()
mode(7)
//Problem initialization in Scilab
help csetup
[x,x_low,x_up,v,cl,cu,equatn,linear] = csetup(probpath+'/OUTSDIF.d', [%t %t %f]);
help cnames
[pname,xnames,gnames] = cnames() //problem, variable and constraint names
[x_low x x_up] // boundaries and initial point
[linear equatn] // 4 linear equality constraints 
[n,m] = cdimen() // number of unknown, number of constraints
cdimsh() //number of nonzeros of the Hessian of the Lagrangian
cdimsj() //number of nonzeros of the Jacobian of the constraints
help cfn
[f,c] = cfn(x) //cost and constraint evaluation a x
[g,cjac] = cgr(x) //gradient of the objective, and gradient of the
                  //general constraint functions 
help cidh
cidh(x,0) //hessian of the objective function
cidh(x,4) //hessian of the fourth constraint
