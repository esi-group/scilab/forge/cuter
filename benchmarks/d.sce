//exec ~/NewOptimization/fsqp-1.x/builder.sce;
//exec ~/NewOptimization/CUTEr-1.x/builder.sce;
exec ~/NewOptimization/fsqp-1.x/loader.sce;
exec ~/NewOptimization/CUTEr-1.x/loader.sce;
NAME='NLMSURF' //'NOBNDTOR'
siffile='~/NewOptimization/CUTEr-1.x/sif/'+NAME+'.SIF';
probpath=TMPDIR+'/'+NAME';
CLASS=part(get_classification(siffile),1:3)
ierrdecode=execstr('sifdecode(siffile,probpath)','errcatch')
ierrbuild=execstr('buildprob(probpath)','errcatch')
props=get_problem_sizes(probpath);
N=props.nfree+props.nfixed+props.nlower+props.nupper+props.nboth;
unbounded=props.nfree==N;
ierrsetup=execstr('[XSTART,bl,bu,v,cl,cu,equatn,linear] = csetup(probpath+''/OUTSDIF.d'',[%t %t %f]);','errcatch')

split=cumsum([1,props.nlineq,props.nlinin,props.nnlineq,props.nnlinin]);
Ilineq   = split(1):split(2)-1; //linear equalities
Ilinin   = split(2):split(3)-1; //linear inequalities
Inlineq  = split(3):split(4)-1; //nonlinear equalities
Inlinin  = split(4):split(5)-1; //nonlinear inequalities

nineqn=size(find([cl(Inlinin);-cu(Inlinin)]>-1d20),'*');
nineq= nineqn+size(find([cl(Ilinin);-cu(Ilinin)]>-1d20),'*');
neqn=props.nnlineq;
neq = neqn+props.nlineq;
ipar=  [1, nineqn ,nineq,neqn,neq,110,2000, 0];
bigbnd=1.e20; eps=1.e-10; epsneq=1e-8; udelta=0;
rpar=[bigbnd,eps,epsneq,udelta];
x=XSTART;

[xopt,inform,fopt,gopt,lambda]=fsqp(XSTART,ipar,rpar,[bl,bu],fsqp_obj,fsqp_cntr,fsqp_grobj,fsqp_grcntr)

