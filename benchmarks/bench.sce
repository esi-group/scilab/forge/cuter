mode(-1)
// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
Tpath=get_absolute_file_path('bench.sce');
exec ~/NewOptimization/CUTEr-1.x/builder.sce;
exec ~/NewOptimization/fsqp-1.x/builder.sce;
exec ~/NewOptimization/quapro-1.x/builder.sce;
exec ~/NewOptimization/sciIpopt/builder.sce;

exec ~/NewOptimization/CUTEr-1.x/loader.sce;
exec ~/NewOptimization/fsqp-1.x/loader.sce;
exec ~/NewOptimization/quapro-1.x/loader.sce
exec ~/NewOptimization/sciIpopt/loader.sce

fname=Tpath+'Results_'+date()+'.dat';
cuter_bench(fname);
//cuter_bench(fname,['optimqn','optimgc','fsqpal','fsqpnl','qld','qpsolve', 'quapro']);
//cuter_bench(fname,'ipopt');
