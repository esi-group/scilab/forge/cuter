function f=cost(i,x)
  f=100*(x(2)-x(1)^2)^2+(1-x(1))^2
endfunction

function g=grcost(i,x)
  g=[-400*(x(2)-x(1)^2)*x(1)-2*(1-x(1))
     200*(x(2)-x(1)^2)]
endfunction


function cj=constr(j,x)
  if j==1 then
    cj=-(-x(1) + x(2)^2)
  else
    cj=-(x(1)^2 - x(2))
  end
endfunction

function gj=grconstr(j,x)
  if j==1 then
    gj=[1;-2*x(2)]
  else
    gj=[-2*x(1);1]
  end
endfunction


function cj=constr2(j,x)
  select j
  case 1 then  cj=-(-x(1) + x(2)^2)
  case 2 then  cj=-(x(1)^2 - x(2))
  case 3 then  cj=(-x(1) + x(2)^2)-10
  case 4 then  cj=(x(1)^2 - x(2))-10
  end
endfunction
bl=[-0.5
    -1d20];
bu=[0.5
    1];
XSTART=[-2;1];
nf=1; nineqn=2; nineq=2; neqn=0; neq=0; modefsqp=100; miter=500; iprint=1;
ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
bigbnd=1.e20; eps=1.e-12; epsneq=0.e0; udelta=0.e0;
rpar=[bigbnd,eps,epsneq,udelta];
[xopt,inform,fopt,gopt,lambda]=fsqp(XSTART,ipar,rpar,[bl,bu],cost,constr,grcost,grconstr)

tkt=gopt.*lambda(3:4)

//the optimum point is at (0,0) with cost value=1
return
//graphical representation
function f=cost1(x1,x2)
  f=100*(x2-x1^2)^2+(1-x1)^2
endfunction


f=gcf();f.color_map=hotcolormap(256);
x1=linspace(-3,5,500);x2=linspace(-10,10,200);
Z=feval(x1,x2,cost1);Z(Z>0.2)=0.2;
clf();grayplot(x1,x2,Z) //the criterion
xfpoly(x2^2,x2);e=gce();e.foreground=-2;e.background=-2;//first constraint curve

xfpoly(x1,x1^2);e=gce();e.foreground=-2;e.background=-2;//second constraint curve

halt
x1=linspace(-0.5,0.5,500);x2=linspace(-1,1,500);
Z=feval(x1,x2,cost1);
Z(Z>10)=10;

clf();grayplot(x1,x2,Z) //the criterion
xx2=linspace(-10,10,2000);
xfpoly(xx2^2,xx2);e=gce();e.foreground=-2;e.background=-2;//first constraint curve
xx1=linspace(-3,5,2000);
xfpoly(xx1,xx1^2);e=gce();e.foreground=-2;e.background=-2;//second constraint curve


