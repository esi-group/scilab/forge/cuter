mode(-1)
// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
ll=lines()
lines(0)

//solvers=['optimqn','optimgc','qld','qpsolve','fsqp']
solvers='fsqp'
Tpath=get_absolute_file_path('get_sif_infos.sce');
benchlib=lib(Tpath+'macros')
fname=Tpath+'Results_'+date();

path=get_sif_path()+'sif';
PBS=stripblanks(basename(listfiles(path+'/*.SIF')));
PBS=gsort(PBS,'g','i');
PBS(or(PBS==['SAMPLE' 'ALLINIT']))=[]; // these makes crash (check why)
//Res_general=benchgeneral();
k1=474;
for k=k1:np
  mprintf("%d/%d, %s: ",k,np,PBS(k))
  siffile=path+'/'+PBS(k)+'.SIF';
  NAME=basename(siffile);
  probpath=TMPDIR+'/'+NAME;
  CLASS=part(get_classification(siffile),1:3);
  ierrdecode=execstr('sifdecode(siffile,probpath)','errcatch');
  if ierrdecode==0 then
    ierrbuild=execstr('buildprob(probpath)','errcatch');
    if ierrbuild==0 then
      props=get_problem_sizes(probpath);
      N=props.nfree+props.nfixed+props.nlower+props.nupper+props.nboth;
      Res_general=[Res_general;benchgeneral(CLASS,NAME,N,props(3:$))];
    end
  else
    mprintf("Problem %s : decode error (%d)\n",NAME,ierrdecode)
  end
  mprintf("done\n")

end //end of loop on problems
