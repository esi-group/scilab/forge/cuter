options=optimset('fmincon');

options.GradObj='on';
options.GradConstr='on';
options.MaxFunEvals=1000;
x0=[5;0.0001;-0.0001];
bigbnd=1.e20;
bl=-bigbnd*ones([3,1]);
bu=bigbnd*ones([3,1]);

[x,fval,exitflag,output,lambda] = fmincon(@byrdsphr_cost,x0,[],[],[],[],bl,bu,@byrdsphr_constr,options)
