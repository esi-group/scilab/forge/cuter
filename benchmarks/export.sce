// ====================================================================
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
// Export all the results into csv data files

path=get_absolute_file_path('export.sce');
// Loads the following variables
// solvers : a list of strings representing the various solvers
// Res_general : the data for the problems
// Res_qld : the results for the qld solver
// Res_qpsolve : the results for the qpsolve solver
// Res_quapro : the results for the quapro solver
// Res_fsqpal : the results for the fsqpal solver
// Res_fsqpnl : the results for the fsqpnl solver
// Res_optimgc : the results for the optimgc solver
// Res_optimqn : the results for the optimqn solver
// Res_ipopt : the results for the ipopt solver
load(path+'all.dat')
solvers=solvers(:)'
mputl(tabul2csv(Res_fsqpal),"fsqpal.csv")
mputl(tabul2csv(Res_fsqpnl),"fsqpnl.csv")
mputl(tabul2csv(Res_general),"general.csv")
mputl(tabul2csv(Res_ipopt),"ipopt.csv")
mputl(tabul2csv(Res_optimgc),"optimgc.csv")
mputl(tabul2csv(Res_optimqn),"optimqn.csv")
mputl(tabul2csv(Res_qld),"qld.csv")
mputl(tabul2csv(Res_qpsolve),"qpsolve.csv")
mputl(tabul2csv(Res_quapro),"quapro.csv")

