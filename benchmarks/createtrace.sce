// Copyright (C) 2010 - DIGITEO - Michael Baudin

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Create the trace for the performance profile of fmincon.

path = get_absolute_file_path("createtrace.sce");
// Loads the following variables
// solvers : a list of strings representing the various solvers
// Res_general : the data for the problems
// Res_qld : the results for the qld solver
// Res_qpsolve : the results for the qpsolve solver
// Res_quapro : the results for the quapro solver
// Res_fsqpal : the results for the fsqpal solver
// Res_fsqpnl : the results for the fsqpnl solver
// Res_optimgc : the results for the optimgc solver
// Res_optimqn : the results for the optimqn solver
// Res_ipopt : the results for the ipopt solver

load(path+"all.dat")

// Export these traces as separate trace data files
result2tracefile ( Res_ipopt , Res_general , "ipopt.trc" );
result2tracefile ( Res_optimqn , Res_general , "optimqn.trc" );
result2tracefile ( Res_optimgc , Res_general , "optimgc.trc" );
result2tracefile ( Res_fsqpnl , Res_general , "fsqpnl.trc" );
result2tracefile ( Res_fsqpal , Res_general , "fsqpal.trc" );
result2tracefile ( Res_quapro , Res_general , "quapro.trc" );
result2tracefile ( Res_qpsolve , Res_general , "qpsolve.trc" );
result2tracefile ( Res_qld , Res_general , "qld.trc" );

