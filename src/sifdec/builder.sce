
// ====================================================================
// Copyright (C) INRIA - Serge Steer
// Copyright (C) DIGITEO - Allan CORNET - 2012
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function build_sifdec()
mode(-1)
Path=get_absolute_file_path('builder.sce');

function adapt_from_ref(Path,nam)
  in = Path+'Reference/'+nam;out=Path+nam;sizing=Path+'sifdec.siz'
  txt = mgetl(in);
  //Modifying some routines names to avaoid v=conflic with Scilab ones
  txt = strsubst(txt,'GETVAL','GETVL1')
  txt = strsubst(txt,'GETLIN','GETLN1')
  
  if nam == 'local.f' then
    //remplacer les valeurs par des appels a dlamch et rlamch
    txt=strsubst(txt,'${R1}','1.1920930E-07')
    txt=strsubst(txt,'${R2}','5.9604646E-08')
    txt=strsubst(txt,'${R3}','1.40131E-45')
    txt=strsubst(txt,'${R4}','1.1754945E-38')
    txt=strsubst(txt,'${R5}','3.4028234E+38')
    txt=strsubst(txt,'${D1}','2.2204460492503132D-16')
    txt=strsubst(txt,'${D2}','1.1102230246251566D-16')
    txt=strsubst(txt,'${D3}','4.9406564584126D-324')
    txt=strsubst(txt,'${D4}','2.225073858507202D-308')
    txt=strsubst(txt,'${D5}','1.797693134862314D+308')
    txt=strsubst(txt,'${NBYTES}','8')

  end
  
  if nam=='sdlanc.f' then
    k=grep(txt,'C#{sizing}')
    if k<>[] then
      txt=[txt(1:k);mgetl(sizing);txt(k+1:$)];
    end
    //array initialization addition.
     k=grep(txt,'      DEBUG  =')//find first instruction
     if k<>[] then
       k=k(1)-1
       txt=[txt(1:k);'      include ''init.h''';txt(k+1:$)];
     end
  end


  //replacing STOP by a call to setsifdecerr and a return
  k=grep(txt,'/stop *$/i','r');
  for i=size(k,'*'):-1:1
    ki=k(i); ti=txt(ki)
    if convstr(part(ti,1))<>'c' then
      ind=1;while part(ti,ind)==' ' do ind=ind+1,end;ind=ind-1
      if convstr(stripblanks(ti))=='stop' then
  txt=[txt(1:ki-1);
       part(' ',ones(1,ind))+['call setsifdecerr('''+basename(nam)+''','+string(i)+')'
        'return']
       txt(ki+1:$)]
      else
  txt=[txt(1:ki-1);
       strsubst(ti,'STOP','then')
       part(' ',ones(1,ind+2))+['call setsifdecerr('''+basename(nam)+''','+string(i)+')'
        'return']
       part(' ',ones(1,ind))+'end if'
       txt(ki+1:$)]
      end
    end
  end

  mputl(txt,out)
endfunction

  ncl=lines();lines(0);
  mprintf(_('---------- Building the SIFDEC library\n'));
  Files=['decode.f';'gps.f';'inlanc.f';'local.f';'mafnad.f';'magrad.f';'makefn.f';
   'makegr.f';'printp.f';'rest.f';'trans.f';'utils.f';'sdlanc.f']';

  if newest(Path+['date_build','builder.sce','sifdecerr.f','sifdec.siz','init.h',Files])==1 then
    clear Path Files 
    return
  end

  for f=Files, adapt_from_ref(Path,f),end
  
  //add local files
  Files=[Files 'sifdecerr.f']
  
  //the following lines are a workaround to avoid Scilab-5.1.1 a bug under
  //windows (fortran runtime not shared). This bug will be fixed in the Scilab-5.2 version
  if getos() == "Windows" then
    [a,b] = getversion()
    if ( find(b=="release") == [] ) then
      ext_libs = ['libifcoremdd' 'libmmdd'];
    else
      ext_libs = ['libifcoremd' 'libmmd'];
    end
  else
    ext_libs = [];
  end

  Files = [Files, 'iset.f', 'dset.f'];
  if execstr('tbx_build_src(''sifdec'',Files,''f'',Path,ext_libs,'''','''',''-I''+Path)','errcatch') ==0 then
    mputl(sci2exp(getdate()),Path+'date_build')
  else
    mprintf("%s\n",lasterror())
  end
  lines(ncl(2))
endfunction

build_sifdec();
clear build_sifdec;
 
