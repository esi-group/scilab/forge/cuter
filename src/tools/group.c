/*====================================================================
 * Copyright (C) INRIA -  Serge Steer
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http:*www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 * ====================================================================*/

/* This is a driver able to define and call a dynamically linked  "group" subroutine */


#include "machine.h"
#include "version.h"
/* next definition taken from AddFunctionInTable.h */

typedef void (*voidf)();
typedef struct {
	char *name; /* function name */
	voidf f; /* pointer on function */
} FTAB;

extern voidf AddFunctionInTable (char *name, int *rep, FTAB *table);  

#if (SCI_VERSION_MAJOR >= 5) && (SCI_VERSION_MINOR >= 3)
  extern voidf GetFunctionByName (char *name, int *rep, FTAB *table);
	#define AddFunctionInTable GetFunctionByName
#endif

#define ARGS_group double *gvalue, int *lgvalu, double *fvalue, double *gpvalu, \
    int *ncalcg, int *itypeg,int *istgpa, int *icalcg, int *ltypeg, \
    int *lstgpa, int *lcalcg, int *lfvalu, int *lgpvlu, int *derivs, \
    int *igstat

FTAB FTab_group[] =
{
	{(char *) 0, (voidf) 0}
};

typedef int * (*groupf)(ARGS_group);

/** the current function fixed by setgroupfonc **/

static groupf groupfonc ; /** the current function fixed by setgroupfonc **/

void C2F(setgroupfonc)(char *name, int *rep)
{
  groupfonc=(groupf) AddFunctionInTable(name,rep,FTab_group  );
}

int C2F(grou1)(ARGS_group)
{

  (*groupfonc)(gvalue, lgvalu, fvalue, gpvalu, ncalcg, itypeg, istgpa, icalcg, 
	       ltypeg, lstgpa, lcalcg, lfvalu, lgpvlu, derivs, igstat);
  return 0;
}
 