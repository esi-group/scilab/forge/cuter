function [f,g,h]=BDEXP(x)
  %the CUTEr BDEXP problem, n must be greater or equal to 4
  global count;
  n=size(x,1);h=[];
  %the function
  f=sum((x(1:n-2)+x(2:n-1)).*exp(-(x(1:n-2)+x(2:n-1)).*x(3:n)));
  count(1)=count(1)+1;
  if nargout>=2 
    count(2)=count(2)+1;
    %the gradient
    g1=(1-(x(1)+x(2)).*x(3)).*exp(-(x(1)+x(2)).*x(3));%diff par rapport a x1
    g2=(1-(x(1)+x(2)).*x(3)).*exp(-(x(1)+x(2)).*x(3))+(1-(x(2)+x(3)).*x(4)).*exp(-(x(2)+x(3)).*x(4));%diff par rapport a x2
 
    if n>4 ,
      gi=(1-(x(2:n-3)+x(3:n-2)).*x(4:n-1)).*exp(-(x(2:n-3)+x(3:n-2)).*x(4:n-1))....
	 +(1-(x(3:n-2)+x(4:n-1)).*x(5:n)).*exp(-(x(3:n-2)+x(4:n-1)).*x(5:n))....
	 -(x(1:n-4)+x(2:n-3)).^(2).*exp(-(x(1:n-4)+x(2:n-3)).*x(3:n-2));
    else
      gi=[];
    end
    gnm1= +(1-(x(n-2)+x(n-1)).*x(n)).*exp(-(x(n-2)+x(n-1)).*x(n))-(x(n-3)+x(n-2)).^(2).*exp(-(x(n-3)+x(n-2)).*x(n-1));
    gn=-(x(n-2)+x(n-1)).^(2).*exp(-(x(n-2)+x(n-1)).*x(n));
    g=[g1;g2;gi;gnm1;gn];
  end

