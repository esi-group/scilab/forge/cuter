// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
//nonlinear constrained problem functions check
//see http://www.netlib.org/opt/lsnno/
function A=CuteSparse(n,H,ICNH,IRNH)
  H=H(:);ICNH=ICNH(:);IRNH=IRNH(:);
  k=find(ICNH<>IRNH)
  A=sparse([[ICNH;IRNH(k)],[IRNH;ICNH(k)]],[H;H(k)],[n,n])
endfunction

function y=Cost(x)
  x1=x(1),x2=x(2),x3=x(3),x4=x(4),x5=x(5);
  y=x2*exp(x1+x3)+(x3*x4)^2+(x3-x5)^2
endfunction
function dy=dCost(x)
  x1=x(1),x2=x(2),x3=x(3),x4=x(4),x5=x(5);
  dy=[x2*exp(x1+x3)
      exp(x1+x3)
      x2*exp(x1+x3)+2*x3*x4 ** 2+2*x3-(2*x5)
      2*x3^2*x4
      -2*x3+2*x5]
endfunction
function d2y=d2Cost(x)
  x1=x(1),x2=x(2),x3=x(3),x4=x(4),x5=x(5);
  d2y(1,1)=x2 * exp(x1 + x3)
  d2y(1,2)=exp(x1 + x3)
  d2y(1,3)=x2 * exp(x1 + x3)
  d2y(1,4)=0
  d2y(1,5)=0
  
  d2y(2,1)=d2y(1,2)
  d2y(2,2)=0
  d2y(2,3)=exp(x1 + x3)
  d2y(2,4)=0
  d2y(2,5)=0
  
  d2y(3,1)=d2y(1,3)
  d2y(3,2)=d2y(2,3)
  d2y(3,3)=x2 * exp(x1 + x3) + 2 * x4^2 + 2
  d2y(3,4)=4 * x3 * x4
  d2y(3,5)=-2

  d2y(4,1)=d2y(1,4)
  d2y(4,2)=d2y(2,4)
  d2y(4,3)=d2y(3,4)
  d2y(4,4)=2 * x3^2
  d2y(4,5)=0

  d2y(5,1)=d2y(1,5)
  d2y(5,2)=d2y(2,5)
  d2y(5,3)=d2y(3,5)
  d2y(5,4)=d2y(4,5)
  d2y(5,5)=2
endfunction


function C=Constr(x)
  x1=x(1),x2=x(2),x3=x(3),x4=x(4),x5=x(5);
  C=[x1+x2-10==0
     -x1-x3+x4==0
     -x2+x3+x5==0
     -x4-x5+10==0
endfunction
function dC=dConstr(x)
 dC=[1  1  0 0 0
      -1 0 -1 1 0
      0  -1 1 0 1
      0  0  0 -1 -1]
endfunction
function d2C=d2Constr(x,i)
 d2C=zeros(5,5)
endfunction

x_low=[2;6;0];
x_up=[4;8;5];
sol=[2;8;0;2;8]
f_opt=123.112




siffile=get_sif_path()+'sif/LSNNODOC.SIF';
probpath=TMPDIR+'/LSNNODOC'
sifdecode(siffile,probpath) //routines are created in the Scilab temporary directory
buildprob(probpath)
[x,bl,bu,v,cl,cu,equatn,linear] = csetup(probpath+'/OUTSDIF.d', [%t %t %f]);
if or(x<>[4;6;2;6;4]) then pause,end
if or(bl(1:3)<>x_low) then pause,end
if or(bu(1:3)<>x_up) then pause,end
if or(v<>zeros(4,1)) then pause,end
if or(cl<>zeros(4,1)) then pause,end
if or(cu<>zeros(4,1)) then pause,end
if or(equatn<>[%t;%t;%t;%t]) then pause,end
if or(linear<>[%t;%t;%t;%t]) then pause,end
[n,m] = cdimen();
if m<>4|n<>5 then pause,end
cdimsh()
cdimsj()
[f,c] = cfn(x);
if or(c<>Constr(x)) then pause,end
if f<>Cost(x)  then pause,end
if f<>cfn(x)  then pause,end
[g,cjac] = cgr(x);
if or(g<>dCost(x))  then pause,end
if or(cjac<>dConstr(x))  then pause,end
[g,cjac,h] = cgrdh(x,v)
if or(g<>dCost(x))  then pause,end
if or(cjac<>dConstr(x))  then pause,end

if or(h<>d2Cost(x))  then pause,end
[g,cjac,h] = cgrdh(x,v,[%t %f])
if or(g<>dCost(x))  then pause,end
if or(cjac<>dConstr(x)')  then pause,end
if or(h<>d2Cost(x))  then pause,end
if or(cidh(x,0)<>d2Cost(x))  then pause,end
if or(cidh(x,4)<>d2Constr(x,4))  then pause,end
//a faire
//[H,ICNH,IRNH] = cish(x,0);
//if or(full(CuteSparse(5,H(:),ICNH(:),IRNH(:)))<>d2Cost(x)) then pause,end
//[H,ICNH,IRNH] = cish(x,1);
//pause

if cnames()<>"LSNNODOC  " then pause,end
[pname,xnames,gnames] = cnames()
if pname<>"LSNNODOC  " then pause,end
if gnames<>"C1        C2        C3        C4        " then pause,end
if xnames<>"X1        X2        X3        X4        X5        " then pause,end
  
[f,g] = cofg(x);
if f<>Cost(x)  then pause,end
if or(g<>dCost(x))  then pause,end
if f<>cofg(x)  then pause,end

if or(cprod(x,v,ones(5,1),%f)<>d2Cost(x)*ones(5,1)) then pause,end
[cjac,indvar,indfun,H,ICNH,IRNH]=csgrsh(x,v,0);
if or(full(CuteSparse(5,H,ICNH,IRNH))<>d2Cost(x)) then pause,end
kobj=find(indfun==0)
if or(g<>matrix(cjac(kobj),-1,1)) then pause,end
cjac(kobj)=[];indvar(kobj)=[];indfun(kobj)=[];
if or(full(sparse([indfun(:) indvar(:)],cjac(:)))<>dConstr(x)) then pause,end


[H,ICNH,IRNH]= csh(x,v);
if or(full(CuteSparse(5,H,ICNH,IRNH))<>d2Cost(x)) then pause,end

if or(cvarty()<>zeros(5,1))  then pause,end
