//to be used with ARGINB.sci
Stop=[1000;1000;1d-13;1d-13];
global count;count=[0,0];
[f1,x1,g1]=optim(ARGLINB_OPTIM,ones(10,1),'qn','ar',Stop(1),Stop(2),Stop(3), ...
		 Stop(4),imp=0);
mprintf('algo=''qn'',cost=%e, gradient norm=%e, cost evaluation=%d, gradient evaluation=%d\n',f1,norm(g1),count(1),count(2))
//algo='qn',cost=4.634146e+00, gradient norm=5.592557e-10, cost evaluation=5, gradient evaluation=5

Stop=[1000;1000;1d-13;1d-13];

global count;count=[0,0];
[f1,x1,g1]=optim(ARGLINB_OPTIM,ones(10,1),'gc','ar',Stop(1),Stop(2),Stop(3), ...
		 Stop(4),imp=0);
mprintf('algo=''gc'',cost=%e, gradient norm=%e, cost evaluation=%d, gradient evaluation=%d\n',f1,norm(g1),count(1),count(2))
//algo='gc',cost=4.634146e+00, gradient norm=7.184404e-05, cost evaluation=72, gradient evaluation=72
