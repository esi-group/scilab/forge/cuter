mode(-1);
// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
curdir=pwd()
chdir(get_absolute_file_path('cleanmacros.sce'))
tokeep=['addnames.sci'
	'benchconmin.sci'
	'benchfsqpal.sci'
	'benchfsqpnl.sci'
	'benchgeneral.sci'
	'benchipopt.sci'
	'benchoptimgc.sci'
	'benchoptimqn.sci'
	'benchqld.sci'
	'benchqpsolve.sci'
	'benchquapro.sci'
	'buildmacros.sce'
	'buildprob.sci'
	'checksolution.sci'
	'cleanmacros.sce'
	'cnames.sci'
	'conmin_constr.sci'
	'conmin_obj.sci'
	'cute2qp.sci'
	'cutedemos.sci'
	'cuter_bench.sci'
	'fsqp_cntr.sci'
	'fsqp_grcntr.sci'
	'fsqp_grobj.sci'
	'fsqp_neweval.sci'
	'fsqp_obj.sci'
	'get_classification.sci'
	'get_problem_sizes.sci'
	'get_sif_path.sci'
	'get_variable_args.sci'
	'ipopt_cntr.sci'
	'ipopt_grcntr.sci'
	'ipopt_grobj.sci'
	'ipopt_obj.sci'
	'optim_obj.sci'
	'perf_profile.sci'
	'pretreat.sci'
	'%probsize_p.sci'
	'projected_gradient.sci'
	'result2trace.sci'
	'results2csv.sci'
	'sifconmin.sci'
	'sifdecode.sci'
	'siffsqp.sci'
	'sifipopt.sci'
	'sifnewuoa.sci'
	'sifoptim.sci'
	'sifqld.sci'
	'sifqpsolve.sci'
	'sifquapro.sci'
	'sifselect.sci'
	'%s_i_tabul.sci'
	'tabul2csv.sci'
	'tabul2string.sci'
	'%tabul_c_tabul.sci'
	'%tabul_e.sci'
	'%tabul_f_tabul.sci'
	'%tabul_p.sci'
	'%tabul_size.sci'
	'test_conmin.sci'
	'test_fsqpal.sci'
	'test_fsqpnl.sci'
	'test_ipopt.sci'
	'test_optimgc.sci'
	'test_optimqn.sci'
	'test_qld.sci'
	'test_qpsolve.sci'
	'test_quapro.sci'
	'unames.sci'
	'unlink.sci'];
files=listfiles('*')';
for k=tokeep',files(files==k)=[];end
for f=files,mdelete(f),end
chdir(curdir)
clear files f mdelete listfiles curdir tokeep
