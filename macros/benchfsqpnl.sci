// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function R=benchfsqpnl(varargin)
  nv=size(varargin)
  select nv
  case 0 then //init
     R=benchfsqpnl([],[],[],[],[],[],[],[])
  case 1 then //failure
    R=benchfsqpnl(%nan,%nan,%nan,%nan,%nan,%nan,%nan,varargin(1))
  case 8 then
    R=mlist(['tabul','Tabname',"Nf","Ng","Cost","feas","glagn","comp_val","Time","Fail"], 'fsqpnl',varargin(:))
  else
    error('invalid number of arguments')
  end
endfunction
