// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function [fopt,xopt,gopt]=sifconmin(siffile) 
// The sifoptim function decode the problem given by a '.sif' file,
// built it and runs the optim function on it
  
// siffile:	path of the .sif file
  
// fopt:        criterion value at xopt
// xopt:	point found
// gopt:	gradient at xopt
// report:      row vector of 8 strings.
//              [probname,probsize,#constraints,NITER,fopt,normG ,fail,stop]
  
//Authors S. Steer. Copyright INRIA

  // Optimizer settings


  //decoding and compiling 
  NAME=basename(siffile)
  probpath=TMPDIR+'/'+NAME
  if ~isdir(probpath)
    unix_s('mkdir '+probpath)
  end
  sifdecode(siffile,probpath) //routines are created in the Scilab temporary directory
  buildprob(probpath)
  
  props=get_problem_sizes(probpath)
  //initialize problem routines data structures
  [XSTART,bl,bu,v,cl,cu,equatn,linear] = csetup(probpath+'/OUTSDIF.d', [%t %t %f]);
  
  if props.nlineq<>0 |props.nnlineq<>0 then
    error('Equality constraints are not handled by conmin")
  end
  split=cumsum([1,props.nlineq,props.nlinin,props.nnlineq,props.nnlinin])
  Ilineq   = split(1):split(2)-1 //linear equalities
  Ilinin   = split(2):split(3)-1 //linear inequalities
  Inlineq  = split(3):split(4)-1 //nonlinear equalities
  Inlinin  = split(4):split(5)-1 //nonlinear inequalities
 
  bnd=[cl(Inlinin);-cu(Inlinin);cl(Ilinin);-cu(Ilinin)];
  bounded = find(bnd>-1d20);
  ncon=size(bounded,'*')
  N = size(XSTART,'*')
  isc=zeros(ncon,1);isc(Ilinin)=1
 
  nacmx=ncon+2*N+1;
  itmax=20000;//The maximum number of iterations.
  scal=ones(XSTART);//Vector of scaling parameters. Not used if nscal==0
  nscal=0; //Do not scale the variables.
  nfdg=0; //all gradient information is supplied by the user.
  icndir=N+1;//Conjugate direction restart parameter.
  fdch=0.01;//Relative change in decision variable X(I) in calculating finite difference gradients. Not used if NFDG == 0.
  fdchm=0.01;//Minimum absolute step in finite difference gradient calculations.  Not used if NFDG == 0.
  ct=-0.1;//Constraint thickness parameter. Not used if NCON = NSIDE = 0.
  ctmin=0.004;//Minimum absolute value of CT considered in the optimization process. Not used if NCON = NSIDE = 0.
  ctl=-0.01;//Constraint thickness parameter for linear and side constraints. Not used if NCON = NSIDE = 0.
  ctlmin=0.001; // Minimum absolute value of CTL considered in the optimization process. Not used if NCON = NSIDE = 0.
  theta=1.0;//Mean value of the push-off factor in the method of feasible directions. Not used if NCON = NSIDE = 0.
  delfun=1d-8;//Minimum relative change in the objective function to indicate convergence.
  dabfun=1d-8;//Minimum absolute change in the objective function to indicate convergence.
  linobj=0;//Linear objective function identifier.
  itrm=5;//Number of consecutive iterations to indicate convergence by relative or absolute changes, DELFUN or DABFUN.
  alphax=0.1;// the maximum fractional change in any component of X as an initial estimate for ALPHA in the one-dimensional search.
  abobj1=0.1;//fractional change attempted as a first step in the one-dimensional search 
  infog=0;
  info=1;
  iprint=2;
  [xopt,fopt,gopt] = conmin(XSTART,conmin_obj,conmin_constr,ncon,nacmx,..
			    bu,bl,itmax,isc,scal,nscal,nfdg,icndir,..
			    fdch,fdchm,ct,ctmin,ctl,ctlmin, ...
			    theta,delfun,dabfun,linobj,itrm,alphax,abobj1,..
			    infog,info,iprint);
    //build the report data 
endfunction
