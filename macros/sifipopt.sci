// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function [xopt,fopt,extra]=sifipopt(siffile,params) 
// The sifoptim function decode the problem given by a '.sif' file,
// built it and runs the optim function on it
  
// siffile:	path of the .sif file
// ipars: [modefsqp,miter,iprint,eps,epsneq]
  
// fopt:        criterion value at xopt
// xopt:	point found
// gopt:	gradient at xopt

//Authors S. Steer. Copyright INRIA
  maxit=5000;
  iprint=0;
  maxsize=1000;
  bigbnd=1.e20; tol=1.e-12; epsneq=1e-8; 
  
  //decoding and compiling 
  NAME=basename(siffile)
  probpath=TMPDIR+'/'+NAME
  CLASS=part(get_classification(siffile),1:3)
  //Decode the problem,routines are created in propath dir
  sifdecode(siffile,probpath)
  buildprob(probpath)
  props=get_problem_sizes(probpath)
  //initialize problem routines data structures
  [XSTART,bl,bu,v,cl,cu,equatn,linear] = csetup(probpath+'/OUTSDIF.d', [%t %t %f]);
  if props.nlineq+ props.nnlineq>size(XSTART,'*') then
    error('Out of Scope')
  end
  split=cumsum([1,props.nlineq,props.nlinin,props.nnlineq,props.nnlinin])
  Ilineq   = split(1):split(2)-1 //linear equalities
  Ilinin   = split(2):split(3)-1 //linear inequalities
  Inlineq  = split(3):split(4)-1 //nonlinear equalities
  Inlinin  = split(4):split(5)-1 //nonlinear inequalities

  constr_lin_type=ones(cl);
  constr_lin_type(Ilineq)=1;
  constr_lin_type(Ilinin)=1;
  var_lin_type=ones(XSTART)
    
  params = init_param();
  params = add_param(params,"hessian_approximation","limited-memory");
  params = add_param(params,"linear_solver","mumps");
  params = add_param(params,"nlp_lower_bound_inf",-bigbnd);
  params = add_param(params,"nlp_upper_bound_inf",bigbnd);
  params = add_param(params,"tol",tol);
  params = add_param(params,"max_iter",maxit);
  params = add_param(params,"constr_viol_tol",epsneq);
  params = add_param(params,'acceptable_tol',10*tol);
  nconstr=size(cl,'*');
    
  [c,cjac,indvar,indfun]=ccfsg(XSTART);
  sp=[indfun,indvar];

  [xopt, fopt, extra]=ipopt(XSTART, ipopt_obj, ipopt_grobj,nconstr, ..
			    ipopt_cntr, ipopt_grcntr, sp,[], [], ..
			    var_lin_type, constr_lin_type,..
			    cu, cl, bl, bu, params);
  
  
endfunction
