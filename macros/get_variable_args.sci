function args=get_variable_args(siffile)
  txt=mgetl(siffile);
  txt=txt(grep(txt,'$-PARAMETER'));
  txt=strsubst(txt,'/ *\$-PARAMETER.*$/','','r');
  selected=find(part(txt,1)==' ');
  [n,selected,tp,names,values]=msscanf(-1,txt,"%c%s %s %s");
  N=unique(names)';
  args=tlist(['sifargs','names','values','selected'],[],list(),[]);
  for nk=N
    l=find(names==nk);
    v=evstr(values(l).');
    i=find(selected(l)==' ');
    args.names($+1)=nk;
    args.values($+1)=v;
    args.selected($+1)=i;
    
  end
endfunction
function %sifargs_p(args)
  txt=[]
  names=args.names;ln=max(length(names))
  values=args.values
  selected=args.selected
  for k=1:size(names,'*')
    v=string(values(k))
    v(selected(k))='*'+v(selected(k))
    txt=[txt;
	 part(names(k),1:ln)+': ['+strcat(v,', ')+']'];
  end
  mprintf('%s\n',txt)
endfunction
    
