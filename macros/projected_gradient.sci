// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function ngp=projected_gradient(xopt,gopt,bl,bu)
//xopt variable vector  at the found optimum
//gopt ojective function gradient at this point
//bl lower bound vector
//bu upper bound vector
//ngp projected gradient  

  ngp = gopt;
  if (size(bl,'*') > 0) then
    //lower bounds
    lac = find(xopt==bl); //active lower bounds
    if (lac ~= []) then ngp(lac) = min(gopt(lac),0);end
    
    //upper bounds
    uac = find(xopt==bu);
    if (uac ~= []) then ngp(uac) = max(gopt(uac),0);end;
    
    //fixed variables
    ngp(find(bl==bu)) = 0;
  end


endfunction 
