// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function %probsize_p(props)
  //overloading function for probsize data structure display
  f=getfield(1,props)
  f=part(f(2:$)',1:8)
  p=getfield(2,props)
  for k=3:14
    p=[p;string(getfield(k,props))]
  end
  mprintf("  %s\n",f+':'+p)
endfunction
    
