// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function [xopt,inform,fopt,gopt,lambda]=siffsqp(siffile,pars) 
// The sifoptim function decode the problem given by a '.sif' file,
// built it and runs the optim function on it
  
// siffile:	path of the .sif file
// ipars: [modefsqp,miter,iprint,eps,epsneq]
  
// fopt:        criterion value at xopt
// xopt:	point found
// gopt:	gradient at xopt

//Authors S. Steer. Copyright INRIA
  bigbnd=1.e20;    udelta=0;
  // Optimizer settings
  if exists('pars','local')==0 then
    modefsqp=110;
    maxit=2000;
    iprint=0;
    eps=1.e-10; 
    epsneq=1e-8; 
  else
    modefsqp=pars(1);
    maxit=pars(2);
    iprint=pars(3);
    eps=pars(4);
    epsneq=pars(5);
  end


  //decoding and compiling 
  NAME=basename(siffile)
  probpath=TMPDIR+'/'+NAME
  CLASS=part(get_classification(siffile),1:3)
  //Decode the problem,routines are created in propath dir
  sifdecode(siffile,probpath)
  buildprob(probpath)
  props=get_problem_sizes(probpath)
  //initialize problem routines data structures
  [XSTART,bl,bu,v,cl,cu,equatn,linear] = csetup(probpath+'/OUTSDIF.d', [%t %t %f]);
  
  split=cumsum([1,props.nlineq,props.nlinin,props.nnlineq,props.nnlinin])
  Ilineq   = split(1):split(2)-1 //linear equalities
  Ilinin   = split(2):split(3)-1 //linear inequalities
  Inlineq  = split(3):split(4)-1 //nonlinear equalities
  Inlinin  = split(4):split(5)-1 //nonlinear inequalities
      
  
  nineqn=size(find([cl(Inlinin);-cu(Inlinin)]>-1d20),'*');
  nineq= nineqn+size(find([cl(Ilinin);-cu(Ilinin)]>-1d20),'*');
  neqn=props.nnlineq;
  neq = neqn+props.nlineq;
  ipar=  [1, nineqn ,nineq,neqn,neq,modefsqp,maxit, iprint];
  rpar=[bigbnd,eps,epsneq,udelta];

  
  set_x_is_new(1);
  [xopt,inform,fopt,gopt,lambda]=fsqp(XSTART,ipar,rpar,[bl,bu],fsqp_obj,fsqp_cntr,fsqp_grobj,fsqp_grcntr)

endfunction
