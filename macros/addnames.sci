function R=addnames(Res,Res_general)
R=mlist(["tabul","Tabname","NAME","Nf","Ng","Cost","feas","glagn","comp_val","Time","Fail"],..
Res.Tabname,Res_general.Name,Res.Nf,Res.Ng,Res.Cost,Res.feas,Res.glagn,Res.comp_val,Res.Time,Res.Fail);
endfunction

