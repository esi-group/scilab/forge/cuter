// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function report=test_optimqn()
  maxiter=4000;
  maxcall=4000;
  maxsize=6000;
  optimstop=list('ar',maxcall,maxiter,1d-14,1d-14); //optim stop parameters

  if or(CLASS==['SUR' 'OUR' 'SBR' 'OBR' 'OBI' 'QUR', 'QBR'])..
	      &props.nnlineq==0&props.nnlinin==0 ..
	      &props.nlineq==0&props.nlinin==0 then   
    if N>maxsize then report=benchoptimqn('N>'+string(maxsize)),return,end
    ierrsetup=execstr('[XSTART,bl,bu,v,cl,cu,equatn,linear] = csetup(probpath+''/OUTSDIF.d'',[%t %t %f]);','errcatch')
    if ierrsetup<>0 then report=benchoptimqn('csetup error:'+string(ierrsetup)),return,end
    unbounded=props.nfree==N;
    if unbounded then //No bounds constraints
      ierr=execstr('[fopt,xopt,gopt]=optim(optim_obj,XSTART,''qn'',optimstop(:))', 'errcatch')
    else // Bounds contraints
      ierr=execstr('[fopt,xopt,gopt]=optim(optim_obj,''b'',bl,bu,XSTART,''qn'',optimstop(:))', 'errcatch')
    end
    if ierr<>0 then
      report=benchoptimqn('solver error:'+string(ierr))
    else
      [time,calls]=ureprt();   
      if isnan(fopt) then
	fail='Nan'
	feas=%nan,glagn=%nan,comp_val=%nan
      else
	fail='ok'
	if calls(2)>=maxcall then fail='maxcall',end
	if time(2)>1800 then fail='time limit',end
	[feasn,lagn,compn] = checksolution(xopt,bl,bu,cl,cu,equatn,linear,props)
      end
      report=benchoptimqn(calls(1),calls(2),fopt,feasn,lagn,compn,time(2),fail)
    end
  else
    report=benchoptimqn('Out of scope'),
  end
endfunction
