// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function report=test_qld()

  qldstop=1d-12; //qld stop parameters
  maxsize=2500;
  //using the calling scope variables
  if or(CLASS==['QUR' 'QBR' 'QLR'])&props.nnlineq==0&props.nnlinin==0 then
    ierrsetup=execstr('[XSTART,bl,bu,v,cl,cu,equatn,linear] = csetup(probpath+''/OUTSDIF.d'',[%t %t %f]);','errcatch')
    if ierrsetup<>0 then report=benchqld('error:'+lasterror()),return,end
    if size(XSTART,'*')>maxsize then report=benchqld('N>'+string(maxsize)),return,end
 
    if execstr('[H,G,f0,A,b,me]=cute2qp(N,cl,cu,equatn,linear)','errcatch')<>0 then
      report=benchqld('cute2qp error:'+lasterror()),
      return
    end
    if and(bl<=-1d20) then bl1=[],else bl1=bl,end
    if and(bu>=1d20) then bu1=[],else bu1=bu,end

    timer(); 
    ierr=execstr('[xopt,lagr,info]=qld(H,G,A,b,bl1,bu1,me,qldstop);','errcatch')
    t=timer()
    if ierr==0 then
      fopt=cfn(xopt);
      fail=''
      if info==2 then fail='Convergence',end
      if info==1 then fail='maxiter reached',end
      ierr=execstr('[feasn,lagn,compn] = checksolution(xopt,bl,bu,cl,cu,equatn,linear,props)','errcatch')
      if ierr<>0 then 
	feasn=%nan,lagn=%nan,compn=%nan
      end
      report=benchqld(fopt,feasn,lagn,compn,t,fail),
    else
      report=benchqld('error:'+lasterror()),
    end
  else
    report=benchqld('Out of scope'),
  end
endfunction

