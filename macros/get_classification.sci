// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function c=get_classification(siffile)
  T=mgetl(siffile);
  T=T(part(T,1)=='*')//the comment lines
  T=strsubst(T,'/^. */','','r')
  k=find(convstr(part(T,1:14))=='classification')
  if k==[] then
    c=[]
  else
    k=k(1)
    c=stripblanks(part(T(k),15:length(T(k))))
  end
endfunction
