// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function [fopt,xopt,gopt]=sifoptim(siffile,algo,Stop,imp) 
// The sifoptim function decode the problem given by a '.sif' file,
// built it and runs the optim function on it
  
// siffile:	path of the .sif file
// algo:	'qn' or 'gc' for quasi-Newton or conjugate gradient
// Stop:        parameters controlling the convergence of the algorithm 
//              Stop=[nap,iter,epsg,epsf] (see optim)
  
// fopt:        criterion value at xopt
// xopt:	point found
// gopt:	gradient at xopt
// report:      row vector of 8 strings.
//              [probname,probsize,#constraints,NITER,fopt,normG ,fail,stop]
  
//Authors B. Durand, S. Steer. Copyright INRIA

  //-- optim settings --
  if exists('imp','local')==0 then imp=0,end
  args=list()
  if exists('algo','local')==1 then 
    args($+1)=algo;
  else
    algo='qn'
  end
  if exists('Stop','local')==1 then 
    args($+1)='ar'
    for k=1:prod(size(Stop))
      args($+1)=Stop(k)
    end
  end
  
  //-- decoding and compiling --
  NAME=basename(siffile)
 
  probpath=TMPDIR+'/'+NAME
  CLASS=part(get_classification(siffile),1:3)
  if and(CLASS<>['SUR' 'OUR' 'SBR' 'OBR' 'OBI' 'QUR', 'QBR']) then
    error(_("The problem Class is out of optim scope"))
  end
  //Decode the problem,routines are created in propath dir
  sifdecode(siffile,probpath)
  buildprob(probpath)

  //-- initialize problem routines data structures --
  [XSTART,BL,BU] = usetup(probpath+'/OUTSDIF.d');
  [NC,fixed,notfixed] = pretreat(BL,BU)
  N = size(XSTART,'*')
  if N>6000&algo=='qn' then
    error(_("The problem is too big for qn method"))
  end
 
  if (NC == 0) then //No bounds constraints
    [fopt,xopt,gopt]=optim(optim_obj,XSTART,args(:),imp=imp)
  else // Bounds contraints
    [fopt,xopt,gopt]=optim(optim_obj,'b',BL,BU,XSTART,args(:),imp=imp)
  end
endfunction
