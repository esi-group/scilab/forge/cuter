// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function [xopt,lagr,info]=sifqld(siffile,tol) 
// The sifqld function decode the problem given by a '.sif' file,
// built it and runs the qpsolve function on it
  
// siffile:	path of the .sif file
// tol:         required precision 
  
// fopt:        criterion value at xopt
// xopt:	point found
// report:      bench data structure
  
//Authors S. Steer. Copyright INRIA


  // Optimizer settings
  if argn(2)<2 then 
    tol=1d-12
  elseif size(tol,'*')<>1 then
    error('Third argument must be a scalar')
  end

  //decoding and compiling 
  NAME=basename(siffile)
  probpath=TMPDIR+'/'+NAME
  CLASS=part(get_classification(siffile),1:3)
  if and(CLASS<>['QUR' 'QBR' 'QLR']) then
    error(_("The problem Class is out of qld scope"))
  end

  //Decode the problem,routines are created in propath dir
  sifdecode(siffile,probpath)

  //check if submitted problem is compatible with qld
  props=get_problem_sizes(probpath)
  if props.nnlineq<>0|props.nnlinin<>0 then 
    error(_("The problem Class is out of qld scope"))
  end
  
  buildprob(probpath)

  //initialize problem routines data structures
  [XSTART,bl,bu,v,cl,cu,equatn,linear] = csetup(probpath+'/OUTSDIF.d', [%t %t %f]);

  N = size(XSTART,'*')
  M=size(cl,1)
  
  //check boundary constraints
  if and(bl<=-1d20) then bl=[],end
  if and(bu>=1d20) then bu=[],end
   
  [H,G,f0,A,b,me]=cute2qp(N,cl,cu,equatn,linear)
  [xopt,lagr,info]=qld(H,G,A,b,bl,bu,me ,tol)

endfunction
