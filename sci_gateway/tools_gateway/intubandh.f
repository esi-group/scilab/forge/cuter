      subroutine intubandh(fname)
c     Author: S. Steer, Copyright INRIA
c     bandh=ubandh(x,nsemib,goth)

      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
      logical GOTH
c
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      if(.not.checkrhs(fname,2,3)) return
      if(.not.checklhs(fname,1,1)) return

      if(.not.getrhsvar(1,'d', Mx, Nx, lX)) return
      if (mx*nx.ne.NVAR) then
          buf='X does not match current problem dimension'
         call error(999)
         return
      endif
      if(.not.getrhsvar(2,'d', Mn, Nn, lN)) return
      NSEMIB=stk(lN)

      GOTH=.false.
      if (rhs.eq.3) then
         if(.not.getrhsvar(3,'b', Mg, Ng, lG)) return
         if(istk(lG).ne.0) GOTH=.true.
      endif
      if(.not.createvar(rhs+1,'d',NSEMIB+1,NVAR, lB)) return
      CALL UBANDH( NVAR, GOTH, stk(lX), NSEMIB, stk(lB), NSEMIB )
      lhsvar(1)=rhs+1
      return
      end
