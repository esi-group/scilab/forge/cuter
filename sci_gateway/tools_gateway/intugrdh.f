      subroutine intugrdh(fname)
c     Author: S. Steer, Copyright INRIA
c     [G,H]=ugrdh(X)
      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
c
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      if(.not.checkrhs(fname,1,1)) return
      if(.not.checklhs(fname,2,2)) return
      if(.not.getrhsvar(1,'d', Mx, Nx, lX)) return
      if (mx*nx.ne.NVAR) then
         buf='X does not match current problem dimension'
         call error(999)
         return
      endif
      if(.not.createvar(2,'d', NVAR, 1, lG)) return
      if(.not.createvar(3,'d', NVAR, NVAR, lDH)) return
      CALL UGRDH( NVAR, stk(lX), stk(lG), NVAR, stk(lDH))
      lhsvar(1)=2
      lhsvar(2)=3
      return
      end
