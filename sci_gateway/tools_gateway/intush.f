      subroutine intush(fname)
c     Author: S. Steer, Copyright INRIA
c     [SH,ICNH,IRNH]=ush1(X)

      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
      integer nnz
c
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      rhs=max(0,rhs)
      if(.not.checkrhs(fname,1,1)) return
      if(.not.checklhs(fname,3,3)) return

      if(.not.getrhsvar(1,'d', Mx, Nx, lX)) return
      if (mx*nx.ne.NVAR) then
         buf='X does not match current problem dimension'
         call error(999)
         return
      endif
      call UDIMSH(nnz)
      if(.not.createvar(2,'d', nnz, 1, lV)) return
      if(.not.createvar(3,'i', nnz, 1, lIR)) return
      if(.not.createvar(4,'i', nnz, 1, lIC)) return
      call USH(NVAR, stk(lX),NNZ,NNZ,stk(lV), istk(lIR), istk(lIC))
      lhsvar(1)=2
      lhsvar(2)=3
      lhsvar(3)=4

      return
      end
