      subroutine intuvarty(fname)
c     Author: S. Steer, Copyright INRIA
c     ivarty=uvarty()

      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
c
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      rhs=max(0,rhs)
      if(.not.checkrhs(fname,0,0)) return
      if(.not.checklhs(fname,1,1)) return

      if(.not.createvar(1,'i', NVAR, 1, lI)) return
      call UVARTY(NVAR, istk(lI) )
      lhsvar(1)=1
      return
      end
 
