      subroutine intcsh(fname)
c     Author: S. Steer, Copyright INRIA
c     [SH,ICNH,IRNH]=csh(x)

      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
      integer nnz
c
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      rhs=max(0,rhs)
      if(.not.checkrhs(fname,2,2)) return
      if(.not.checklhs(fname,1,3)) return
      if(.not.getrhsvar(1,'d', Mx, Nx, lX)) return
      if (mx*nx.ne.NVAR) then
         buf='X does not match current problem dimension'
         call error(999)
         return
      endif
      if(.not.getrhsvar(2,'d', Mv, Nv, lV)) return
      if (mv*nv.ne.M) then
         buf='Incorrect dimension'
         call error(999)
         return
      endif
      call CDIMSH(nnz)
      if(.not.createvar(3,'d', nnz, 1, lH)) return
      if(.not.createvar(4,'i', nnz, 1, lIR)) return
      if(.not.createvar(5,'i', nnz, 1, lIC)) return
      call CSH(NVAR, M, stk(lX),M,stk(lV),nnz,nnz, stk(lh),
     $     istk(lIR), istk(lIC))
      lhsvar(1)=3
      lhsvar(2)=4
      lhsvar(3)=5

      return
      end
