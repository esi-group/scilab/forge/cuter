      subroutine intcreprt(fname)
c     Author: S. Steer, Copyright INRIA
c     [time,calls]=creprt();    
      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
c
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      rhs=max(0,rhs)
      if(.not.checkrhs(fname,0,0)) return
      if(.not.checklhs(fname,1,2)) return
      if(.not.createvar(1,'r', 1, 2, lT)) return
      if(.not.createvar(2,'r', 1, 7, lC)) return
      call CREPRT(sstk(lC), sstk(lT))
      lhsvar(1)=1
      lhsvar(2)=2
      return
      end
