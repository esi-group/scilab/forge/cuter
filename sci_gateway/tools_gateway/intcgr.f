      subroutine intcgr(fname)
c     Author: S. Steer, Copyright INRIA
c      [G,Cjac]=cgr(X,V,options)
      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
      logical jtrans,grlagf
c
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      if(.not.checkrhs(fname,1,3)) return
      if(.not.checklhs(fname,1,2)) return
      if(.not.getrhsvar(1,'d', Mx, Nx, lX)) return
      if (mx*nx.ne.NVAR) then
         buf='X does not match current problem dimension'
         call error(999)
         return
      endif
      jtrans = .false.
      grlagf = .false.
      if(rhs.ge.2) then
         if(.not.getrhsvar(1,'d', Mv, Nv, lV)) return
         grlagf=.true.
      endif
      if(rhs.eq.3) then
         if(.not.getrhsvar(1,'b', 1, 2, lO)) return
         JTRANS=istk(lO).ne.0
         GRLAGF=istk(lO+1).ne.0
      endif
      if ( .not. jtrans ) then
         lc1 = m
         lc2 = nvar
      else
         lc1 = nvar
         lc2 = m
      end if
      if(.not.createvar(2,'d', NVAR, 1, lG)) return
      if(.not.createvar(3,'d', lc1, lc2, lCJAC)) return

      CALL CGR(NVAR,M,stk(lX),GRLAGF, 1, stk(lV) ,
     *     stk(lG),JTRANS, LC1, LC2,  stk(lCJAC) )
      lhsvar(1)=2
      lhsvar(2)=3
      return
      end
