      subroutine intsifdec()
C     IALGOR specify the method to be used (1=SBMIN,2=AUGLG,3=BARIA).
C     IPRINT specify whether the problem should be described(<0=DEBUG,0=NO,>0=YES)
C     IAUTO  specify whether the derivatives are supplied or are to be computed
C     IADO   using automatic differentiation and specify whether AD01 or AD02
C            should  be used to perform the automatic differentiation
C
C     Prec   Specify the precision of the output files (single=0,double=1)

      include 'stack.h'

      logical getrhsvar,createvar
      logical checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
      character fname*6
      integer sifu(12),mode(2)
      character path*128
c
      EXTERNAL      SDLANC
      logical SINGLE,NONAME
      integer  getsifdecerr
      external getsifdecerr
c
      NONAME = .FALSE.
      fname='sifdec'
c
      if(.not.checkrhs(fname,3,3)) return
      if(.not.checklhs(fname,1,1)) return
c
      if(.not.getrhsvar(1,'c', M, N, lPi)) return
      ni=m*n
      if(.not.getrhsvar(2,'c', M, N, lPo)) return
      no=m*n
      path=cstk(lPo:lPo+no-1)
c     flags=[IALGOR IPRINT IAUTO IADO Prec]
      if(.not.getrhsvar(3,'i', M, N, lopt)) return
      if(M*N.ne.5) then
         err=3
         call error(89)
         return
      endif

      IALGOR = istk(lopt)
      IPRINT = istk(lopt+1)
      IAUTO = istk(lopt+2)
      IAD0 = istk(lopt+3) 
      SINGLE = istk(lopt+4) .EQ. 0
c
      IOUTFF = 0 
      IOUTGF = 0 
c
c     openig files
      call iset(12,0,sifu,1)
c
c     input file PRBDAT
      mode(1)=1
      mode(2)=0
      call clunit(sifu(1),cstk(lPi:lPi+ni-1),mode)
      if(err.gt.0) goto  99
c     output files
      mode(1)=3
      mode(2)=0
c     PRBOUT = 'OUTSDIF.d'
      path(no+1:no+9)='OUTSDIF.d'
      call clunit(sifu(2),path(1:no+9),mode)
      if(err.gt.0) goto  99
c     PRBRA  = 'RANGE.f'
      path(no+1:no+9)='RANGE.f  '
      call clunit(sifu(3),path(1:no+7),mode)
      if(err.gt.0) goto  99
c     PRBEX  = 'EXTER.f'
      path(no+1:no+9)='EXTER.f  '
      call clunit(sifu(4),path(1:no+7),mode)
      if(err.gt.0) goto  99
      if(iauto.eq.0) then
c     PRBFN  = 'ELFUN.f'
         path(no+1:no+9)='ELFUN.f  '
         call clunit(sifu(5),path(1:no+7),mode)
         if(err.gt.0) goto  99
c     PRBGR  = 'GROUP.f'
         path(no+1:no+9)='GROUP.f  '
         call clunit(sifu(6),path(1:no+7),mode)
         if(err.gt.0) goto  99
      else
         if(IOUTFF.GT.0) then
            path(no+1:no+9)='ELFUNF.f '
            call clunit(sifu(7),path(1:no+8),mode)
            if(err.gt.0) goto  99
         endif
         path(no+1:no+9)='ELFUND.f '
         call clunit(sifu(8),path(1:no+8),mode)
         if(err.gt.0) goto  99
         if(IOUTGF.gt.0) then
            path(no+1:no+9)='GROUPF.f '
            call clunit(sifu(9),path(1:no+8),mode)
            if(err.gt.0) goto  99
         endif
         path(no+1:no+9)='GROUPD.f '
         call clunit(sifu(10),path(1:no+8),mode)
         if(err.gt.0) goto  99
         path(no+1:no+9)='EXTERA.f '
         call clunit(sifu(11),path(1:no+8),mode)
         if(err.gt.0) goto  99
      endif
c     IOUTEM?
      path(no+1:no+9)='OUTMESS  '
      call clunit(sifu(12),path(1:no+7),mode)
      if(err.gt.0) goto  99
c
      IINGPS=sifu(1)
      IOUTDA=sifu(2)
      IOUTRA=sifu(3)
      IOUTEX=sifu(4)
      IOUTFN=sifu(5)
      IOUTGR=sifu(6)
c
      IINFN  = IINGPS
      IINGR = IINGPS
      IINEX = IINGPS
      IOUTFF = sifu(7)
      IOUTGF = sifu(8)
      IOUTFD = sifu(9)
      IOUTGD = sifu(10)
      IOUTEA = sifu(11)
      IOUTEM = sifu(12)
      INFORM=0
c
      IOUT = IOUTEM
      if (IOUTFD.eq.0) IOUTFD=97
      if (IOUTGD.eq.0) IOUTGD=98
      if (IOUTEA.eq.0) IOUTEA=99
      INFORM=0
      call setsifdecerr('  ',0)
      CALL SDLANC( IINGPS, IOUTDA, IINFN , IOUTFN, IOUTFF, IOUTFD, 
     *             IOUTRA, IINGR , IOUTGR, IOUTGF, IOUTGD, 
     *             IINEX , IOUTEX, IOUTEM, IOUTEA, IPRINT, IOUT  , 
     *             NONAME, IALGOR, IAUTO , IAD0  , SINGLE, INFORM )
      do  i=1,12
         if(sifu(i).gt.0) call clunit(-sifu(i), ' ',mode)
      enddo

      IF ( INFORM .NE. 0.or.getsifdecerr().ne.0) THEN
         write(buf,'(''sifdec internal error, inform='',I4)') INFORM
         call error(1111)
      endif
      lhsvar(1)=0
      return
c
 99   continue
      do  i=1,12
         if(sifu(i).gt.0) call clunit(-sifu(i), ' ',mode)
      enddo
      call error(err)
      return
      end
