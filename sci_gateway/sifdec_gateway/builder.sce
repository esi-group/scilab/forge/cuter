// ====================================================================
// Copyright (C) INRIA - Serge Steer
// Copyright (C) DIGITEO - Serge Steer - 2012
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function builder_gw_sifdec()

mode(-1)
mprintf(_('---------- Building the SIFDEC gateway\n'));
path=get_absolute_file_path('builder.sce');

src_files='intsifdec.f'

if newest(path+['date_build','builder.sce',src_files])==1 then
  return
end

libs='../../src/sifdec/libsifdec'  //must be a relative path

table =['sifdec' 'C2F(intsifdec)'];//C2F used because function gateway is written in Fortran

try
  tbx_build_gateway('gwsifdec',table,src_files,path,libs,"","")
  mputl(sci2exp(getdate()),path+'date_build')
catch
  mprintf("%s\n",lasterror())
end
endfunction

builder_gw_sifdec();
clear builder_gw_sifdec;
