// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2009 - INRIA - Serge STEER <serge.steer@scilab.org>
// Copyright (C) 2012 - DIGITEO - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

mode(-1);

function main_builder()
  try
    getversion("scilab");
  catch
    error(gettext("Scilab 5.4 or more is required."));
  end
  // ====================================================================
  if ~isdef('tbx_builder_src') then
    error(msprintf(gettext('%s module not installed."),'modules_manager'));
  end
  // ====================================================================
  TOOLBOX_NAME  = "CUTEr";
  TOOLBOX_TITLE = "CUTEr";
  // ====================================================================
  toolbox_dir = get_absolute_file_path("builder.sce");
  tbx_builder_src(toolbox_dir);
  tbx_builder_gateway(toolbox_dir);
  tbx_builder_macros(toolbox_dir);
  tbx_builder_help(toolbox_dir);
  tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
  tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);
endfunction
// ====================================================================
main_builder();
clear main_builder;
// ====================================================================
