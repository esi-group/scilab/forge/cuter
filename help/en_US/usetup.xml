<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xml:id="usetup" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns5="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>17-Mar-2003</pubdate>
  </info>

  <refnamediv>
    <refname>usetup</refname>

    <refpurpose>unconstrained tool, set up strating point, and
    bounds</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>[X,BL,BU] = usetup(outsdif_path)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>outsdif_path</term>

        <listitem>
          <para>string the path of the problem relative OUTSDIF.d file (see
          <link linkend="sifdecode"> sifdecode</link> )</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>X</term>

        <listitem>
          <para>real vector, starting point of the optimization.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>BL</term>

        <listitem>
          <para>real vector, lower bound on the variables.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>BU</term>

        <listitem>
          <para>real vector, upper bound on the variables.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>usetup sets up the correct data structures for subsequent
    computations in the case where the only possible constraints are bound
    constraints. Those data structures are read in the file OUTSDIF.d produced
    by sifdecode. buildprob must be used before using usetup. A call to usetup
    must precede calls to other unconstrained tools.</para>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="buildprob"> buildprob </link></member>

      <member><link linkend="unames"> unames </link></member>

      <member><link linkend="ufn"> ufn </link></member>

      <member><link linkend="ugr"> ugr </link></member>

      <member><link linkend="uofg"> uofg </link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example">
p=get_sif_path();
sifdecode(p+'sif/BROWNDEN.SIF',TMPDIR+'/BROWNDEN')
buildprob(TMPDIR+'/BROWNDEN')
[X,BL,BU]=usetup(TMPDIR+'/BROWNDEN/OUTSDIF.d')
  </programlisting>
  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Bruno Durand, INRIA</member>

      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Bibliography</title>

    <para>Based on CUTEr authored by</para>

    <para>Nicholas I.M. Gould - n.gould@rl.ac.uk - RAL</para>

    <para>Dominique Orban - orban@ece.northwestern.edu - Northwestern</para>

    <para>Philippe L. Toint - Philippe.Toint@fundp.ac.be - FUNDP</para>

    <para>see http://hsl.rl.ac.uk/cuter-www</para>
  </refsection>
</refentry>