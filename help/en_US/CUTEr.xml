<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xml:id="CUTEr" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns5="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>17-Mar-2003</pubdate>
  </info>

  <refnamediv>
    <refname>CUTEr</refname>

    <refpurpose>Testing environment for optimization and linear algebra
    solvers.</refpurpose>
  </refnamediv>

  <refsection>
    <title>Description</title>

    <para>CUTEr is a versatile testing environment for optimization
    and linear algebra solvers. This toolbox includes function to
    decode and build SIF problems.  It provides CUTEr functions which
    compute the objective, gradients of the objective, contraints,
    jacobian of the constraints and hessians for constrained and
    unconstrained problems. It also provides interfaces with the
    optimization solvers available in Scilab and its toolboxes and
    finnaly a set of tools for benchmarking and computing performance
    profiles.</para>

    <variablelist>
      <varlistentry>
        <term>Requirements</term>

        <listitem>
          <para>A Fortran compiler is mandatory to build problems.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>Toolbox organisation</term>

        <listitem>
          <para> This toolbox contains many parts:</para>

          <variablelist>
            <varlistentry>
              <term>Problem database</term>

              <listitem>
                <para>A set of testing problems coded in "Standard Input
                Format" (SIF) is included in the sif/ sub-directory. This set
                comes from www.numerical.rl.ac.uk/cute/mastsif.html.</para>

                <para>The Scilab function <link
                linkend="sifselect">sifselect</link> can be used to select
                some of this problems according to objective function
                properties, contraints properties and regularity
                properties</para>
              </listitem>
            </varlistentry>

            <varlistentry>
              <term>sif format decoder</term>

              <listitem>
                <para>The Scilab function <link
                linkend="sifdecode">sifdecode</link> can be used to generate
                the Fortran codes associated to a given problem, while The
                Scilab function <link linkend="buildprob">buildprob</link>
                compiles and dynamically links these fortran code with
                Scilab</para>
              </listitem>
            </varlistentry>

            <varlistentry>
              <term>problem relative functions</term>

              <listitem>
                <para>The execution of the function <link
                linkend="buildprob">buildprob</link> adds a set of functions
                to Scilab. The first one is <link
                linkend="usetup">usetup</link> for unconstrained or bounded
                problems or <link linkend="csetup">csetup</link> for problems
                with general contraints. These functions are to be called
                before any of the following to initialize the problem relative
                data (only one problem can be run at a time).</para>
		<para>
		  The functions 
		  <link linkend="udimen">udimen</link>, <link linkend="cdimen">cdimen</link>,
		  <link linkend="udimsh">udimsh</link>,  <link linkend="cdimsh">cdimsh</link>, 
		  <link linkend="unames">unames</link>, <link linkend="cnames">cnames</link>,
		  <link linkend="uvarty">uvarty</link>, <link linkend="cvarty">cvarty</link>, 
		  <link linkend="ureprt">ureprt</link>, <link linkend="creprt">creprt</link> and
		  <link linkend="cdimsj">cdimsj</link>, 
		  <link linkend="get_classification">get_classification</link>, 
		  <link linkend="get_problem_sizes">get_problem_sizes</link>, 
		  can be used to query problem properties.
		</para>
                <para>The other functions allow to compute the objective, the
                gradient, the hessian values, ... of the problem at a given
                point:
		<link linkend="ufn">ufn</link>, <link linkend="cfn">cfn</link>, 
		<link linkend="uofg">uofg</link>, <link linkend="cofg">cofg</link>
		
		<link linkend="ugr">ugr</link>, <link linkend="cgr">cgr</link>, 
		<link linkend="ugrdh">ugrdh</link>, <link linkend="cgrdh">cgrdh</link>, 
		<link linkend="ugrsh">ugrsh</link>, <link linkend="csgrsh">cgrdh</link>, 
		<link linkend="udh">udh</link>, <link linkend="cdh">cdh</link>, <link linkend="cidh">cidh</link>,
		<link linkend="csgr">csgr</link>, <link linkend="ccfg">ccfg</link>, <link linkend="ccfsg">ccfsg</link>,
		<link linkend="ccifg">ccifg</link>, <link linkend="ccifsg">ccifsg</link>.
		</para>
              </listitem>
            </varlistentry>

            <varlistentry>
              <term>CUTEr and optim</term>

              <listitem>
                <para>The Scilab function <literal>
                optim</literal> can be used together with
                CUTEr using either the external function <link
                linkend="optim_obj">optim_obj</link> or the driver
                function <link linkend="sifoptim">sifoptim</link>
                which try to minimize a problem given by its SIF file
                using <literal>optim</literal></para>
              </listitem>
            </varlistentry>

            <varlistentry>
              <term>CUTEr and qld</term>

              <listitem>
                <para>The Scilab function <literal>qld</literal> can be used together with
                CUTEr using either the function <link
                linkend="cute2qp">cute2qp</link> or the driver
                function <link linkend="sifqld">sifqld</link> which
                try to minimize a problem given by its SIF file using
                <literal>qld</literal>.</para>
              </listitem>
            </varlistentry>

            <varlistentry>
              <term>CUTEr and qpsolve</term>

              <listitem>
                <para>The Scilab function <literal>qpsolve</literal> can be used together with
                CUTEr using either the function <link
                linkend="cute2qp">cute2qp</link> or the driver
                function <link linkend="sifqpsolve">sifqpsolve</link> which
                try to minimize a problem given by its SIF file using
                <literal>qpsolve</literal>.</para>
              </listitem>
            </varlistentry>

	    <varlistentry>
              <term>CUTEr and quapro</term>

              <listitem>
                <para>The Scilab function <literal>quapro</literal>
                (Scilab toolbox) can be used together with CUTEr using
                either the function <link
                linkend="cute2qp">cute2qp</link> or the driver
                function <link linkend="sifquapro">sifquapro</link>,
                which try to minimize a problem given by its SIF file
                using <literal>quapro</literal>.</para>
              </listitem>
            </varlistentry>

	    <varlistentry>
              <term>CUTEr and fsqp</term>

              <listitem>
                <para>The Scilab function <literal>fsqp</literal>
                (Scilab toolbox) can be used together with CUTEr using
                either the external functions 
		<link linkend="fsqp_obj">fsqp_obj</link>,  
		<link linkend="fsqp_grobj">fsqp_grobj</link>,  
		<link linkend="fsqp_cntr">fsqp_cntr</link>,  
		<link linkend="fsqp_cntr">fsqp_cntr</link>,  
		<link linkend="fsqp_grcntr">fsqp_grcntr</link>,  

		or the driver
                function <link linkend="siffsqp">siffsqp</link>,
                which try to minimize a problem given by its SIF file
                using <literal>fsqp</literal>.</para>
              </listitem>
            </varlistentry>

	    <varlistentry>
              <term>CUTEr and ipopt</term>

              <listitem>
                <para>The Scilab function <literal>ipopt</literal>
                (Scilab toolbox) can be used together with CUTEr using
                either the external functions <link
                linkend="ipopt_obj">ipopt_obj</link>, <link
                linkend="ipopt_grobj">ipopt_grobj</link>, <link
                linkend="ipopt_cntr">ipopt_cntr</link>, <link
                linkend="ipopt_cntr">ipopt_cntr</link>, <link
                linkend="ipopt_grcntr">ipopt_grcntr</link>, or the
                driver function <link
                linkend="sifipopt">sifipopt</link>, which try to
                minimize a problem given by its SIF file using
                <literal>ipopt</literal>.</para>
              </listitem>
            </varlistentry>

	    <varlistentry>
              <term>CUTEr and conmin</term>

              <listitem>
                <para>
		  The Scilab function <literal>conmin</literal>
		  (Scilab toolbox) can be used together with CUTEr
		  using either the external functions <link
		  linkend="conmin_obj">conmin_obj</link>, <link
		  linkend="conmin_constr">conmin_constr</link> or the
		  driver function <link
		  linkend="sifconmin">sifconmin</link>, which try to
		  minimize a problem given by its SIF file using
		  <literal>conmin</literal>.
	      </para>
              </listitem>
            </varlistentry>
	    <varlistentry>
              <term>Benchmarking tools</term>
              <listitem>
		<para>
		  This toolbox also provides the <link
		  linkend="cuter_bench">cuter_bench</link> function
		  which allow to automatically run the Scilab solvers
		  (optim, fsqp, ipopt, qld, quapro, qpsolve) on all
		  SIF problems provided in this toolbox and to
		  validate the results and to produce an result
		  tables. This function uses the subsidiary functions
		  <literal>test_&lt;solver_name&gt;</literal> to apply a solver to a problem and the 
		  <literal>bench&lt;solver_name&gt;</literal> to manage the result tables.
		</para>
		<para> 
		  The results tables are Scilab
		  <literal>mlist</literal> whose names are
		  Res_&lt;solver_name&gt;. Each row contains the data
		  for a SIF problem. The data fields depends on the
		  solver but there is at least : the optimal objective
		  found (<literal>Cost</literal>,the feasibility norm
		  (<literal>feas</literal>), the lagrangian norm
		  (<literal>glagn</literal>), the dual feasibility
		  norm (<literal>comp_val</literal>), the cpu time
		  (<literal>Time</literal>) and a failure indicator
		  (<literal>Fail</literal>). If relevant, the number
		  of objective function calls (<literal>Nf</literal>)
		  and the number of gradient of objective function
		  calls (<literal>Ng</literal>) are also reported.
		</para>
		<para> 
		  The main properties of SIF problems used are also
		  reported in the <literal>Res_general</literal> data
		  structure.
		</para>
		<para> 
		</para>

		<para>
		  The result tables can then be used to derive performance profiles using the <link
		  linkend="perf_profile">perf_profile</link> function.
		</para>

             </listitem>
            </varlistentry>

          </variablelist>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

 <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Bruno Durand, INRIA</member>

      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Bibliography</title>

    <para>Based on CUTEr authored by</para>

    <para>Nicholas I.M. Gould - n.gould@rl.ac.uk - RAL</para>

    <para>Dominique Orban - orban@ece.northwestern.edu - Northwestern</para>

    <para>Philippe L. Toint - Philippe.Toint@fundp.ac.be - FUNDP</para>

    <para>see http://hsl.rl.ac.uk/cuter-www</para>
  </refsection>
</refentry>
